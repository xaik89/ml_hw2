# ===============================================================================
# Imports
# ===============================================================================

import abstract
from utils import INFINITY, run_with_limited_time, ExceededTimeError
from Reversi.consts import EM, OPPONENT_COLOR, BOARD_COLS, BOARD_ROWS, X_PLAYER
import time
import copy
from collections import defaultdict
import format_open


# ===============================================================================
# Player
# ===============================================================================

class Player(abstract.AbstractPlayer):
    last_pos_moves = None
    maybe_in_book = True
    Corner_squares = [(0,0), (0,7), (7,0), (7,7)]
    X_squares = [(1,1), (1,6), (6,6), (6,1)]
    C_squares = [(0,1), (0,6), (1,0), (1,7), (6,0), (6,7), (7,6), (7,1)]

    def _corner_is_our(self, square, state):
        x, y = square
        x_corner = 0 if x == 0 or x == 1 else 7
        y_corner = 0 if y == 0 or y == 1 else 7
        return state.board[x_corner][y_corner] == self.color

    def _get_value(self, square, state):
        if square in self.Corner_squares:
            return 10
        if square in self.X_squares:
            if self._corner_is_our(square, state):
                return 1
            else:
                return -2
        if square in self.C_squares:
            if self._corner_is_our(square, state):
                return 1
            else:
                return -1

        return 1


    def __init__(self, setup_time, player_color, time_per_k_turns, k):
        abstract.AbstractPlayer.__init__(self, setup_time, player_color, time_per_k_turns, k)
        self.clock = time.time()

        # We are simply providing (remaining time / remaining turns) for each turn in round.
        # Taking a spare time of 0.05 seconds.
        self.turns_remaining_in_round = self.k
        self.time_remaining_in_round = self.time_per_k_turns
        self.time_for_current_move = self.time_remaining_in_round / self.turns_remaining_in_round - 0.05

        self.dict_moves = format_open.TopOpens()
        self.move_number = 1 if self.color == X_PLAYER else 2

    def get_move(self, game_state, possible_moves):
        self.clock = time.time()
        self.time_for_current_move = self.time_remaining_in_round / self.turns_remaining_in_round - 0.05
        if len(possible_moves) == 1:
            return possible_moves[0]

        next_state = copy.deepcopy(game_state)
        best_move = None
        if self.maybe_in_book:
            x, y = self.opening_move(next_state)
            if x is not None:
                new_state = copy.deepcopy(game_state)
                new_state.perform_move(x, y)
                self.last_pos_moves = new_state.get_possible_moves()
                best_move = (x, y)
            else:
                self.maybe_in_book = False

        if best_move is None:
            best_move = possible_moves[0]
            next_state.perform_move(best_move[0], best_move[1])
            # Choosing an arbitrary move
            # Get the best move according the utility function
            for move in possible_moves:
                new_state = copy.deepcopy(game_state)
                new_state.perform_move(move[0], move[1])
                if self.utility(new_state) > self.utility(next_state):
                    next_state = new_state
                    best_move = move

        if self.turns_remaining_in_round == 1:
            self.turns_remaining_in_round = self.k
            self.time_remaining_in_round = self.time_per_k_turns
        else:
            self.turns_remaining_in_round -= 1
            self.time_remaining_in_round -= (time.time() - self.clock)

        return best_move

    def utility(self, state):

        my_u = 0
        op_u = 0
        my_new_u = 0
        op_new_u = 0
        for x in range(BOARD_COLS):
            for y in range(BOARD_ROWS):
                if state.board[x][y] == self.color:
                    my_u += 1
                    my_new_u += self._get_value((x, y), state)
                if state.board[x][y] == OPPONENT_COLOR[self.color]:
                    op_u += 1
                    op_new_u += self._get_value((x, y), state)

        if my_u == 0:
            # I have no tools left
            return -INFINITY
        elif op_u == 0:
            # The opponent has no tools left
            return INFINITY

        len_pos_moves = len(state.get_possible_moves())

        if len_pos_moves == 0:
            return INFINITY if my_u - op_u > 0 else -INFINITY
        if self.color != state.curr_player:
            len_pos_moves = -len_pos_moves

        return my_new_u - op_new_u + len_pos_moves/2

    def selective_deepening_criterion(self, state):
        # Simple player does not selectively deepen into certain nodes.
        return False

    def no_more_time(self):
        return (time.time() - self.clock) >= self.time_for_current_move

    def opening_move(self, state):
        x = None
        y = None
        if self.last_pos_moves is not None:
            for move in self.last_pos_moves:
                if state.board[move[0]][move[1]] == OPPONENT_COLOR[self.color]:
                    x = move[0]
                    y = move[1]
                    break
        elif self.color != X_PLAYER:
            for i, j in [(2, 4), (3, 5), (4, 2), (5, 3)]:
                if state.board[i][j] == OPPONENT_COLOR[self.color]:
                    x = i
                    y = j
                    break

        return self.dict_moves.get_next(x, y)

    def __repr__(self):
        return '{} {}'.format(abstract.AbstractPlayer.__repr__(self), 'better')

# c:\python35\python.exe run_game.py 3 3 3 y simple_player random_player
